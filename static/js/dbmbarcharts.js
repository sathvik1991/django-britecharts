var dbm_endpoint = '/api/chart/data/dbm'
var dbm_daily_data = [];
var dbm_daily_lable = [];
var dbm_months_data = [];
var dbm_months_lable = [];
var dbm_target;
var dbm_yearly_data = [];
var dbm_yearly_lable = [];
var dbm_yearly_target;
$.ajax({
    method: "GET",
    url: dbm_endpoint,
    success: function(dbm_data) {
        dbm_daily_lable = dbm_data.daily_lable
        dbm_daily_data = dbm_data.daily_data
        dbm_months_data = dbm_data.months_data
        dbm_months_lable = dbm_data.months_lable
        dbm_target = dbm_data.target
        dbm_yearly_data = dbm_data.year_data
        dbm_yearly_lable = dbm_data.year_lable
        dbm_yearlu_target = dbm_data.year_target
        setChart()
    },
    error: function(error_data) {
        console.log("error")
        console.log(error_data)
    }
})

function setChart() {
    var YEAR = ["F15", "F16", "F17", "F18", "F19"];
    var color = Chart.helpers.color;
    var yearlydata = {
        labels: dbm_yearly_lable,
        datasets: [{
            label: 'Percentage',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            data: dbm_yearly_data
        }, {
            label: 'Target',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            data: dbm_yearlu_target,
            type: 'line',
            fill: false,
        }]
    };
    var ctx = document.getElementById("dbmyearlycanvas").getContext("2d");
    var yearlychart = new Chart(ctx, {
        type: 'bar',
        data: yearlydata,
        options: {
            showAllTooltips: false,
            elements: {
                rectangle: {
                    borderWidth: 1,
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                position: 'bottom',
                text: 'Yearly Trend'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 80,
                        suggestedMax: 110
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Financial Year'
                    }
                }]
            }
        }
    });
    var montlyBarChartData = {
        labels: months_lable,
        datasets: [{
            label: 'Percentage',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            data: dbm_months_data
        }, {
            label: 'Target',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            data: Array(12).fill(dbm_target),
            type: 'line',
            fill: false,
        }]
    };
    var ctx1 = document.getElementById("dbmmontlycanvas").getContext("2d");
    var montlychart = new Chart(ctx1, {
        type: 'bar',
        data: montlyBarChartData,
        options: {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each horizontal bar to be 2px wide
            showAllTooltips: false,
            elements: {
                rectangle: {
                    borderWidth: 1,
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                position: 'bottom',
                text: 'Monthly Trend'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 80,
                        suggestedMax: 110
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }]
            }
        }
    });
    var dailyBarChartData = {
        labels: dbm_daily_lable,
        datasets: [{
            label: 'Percentage',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            data: dbm_daily_data,
        }, {
            label: 'Target',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            data: Array(30).fill(dbm_target),
            type: 'line',
            fill: false,
        }]
    };
    var dailyctx = document.getElementById("dbmdailycanvas").getContext("2d");
    var dailychart = new Chart(dailyctx, {
        type: 'bar',
        data: dailyBarChartData,
        options: {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each horizontal bar to be 2px wide
            showAllTooltips: false,
            elements: {
                rectangle: {
                    borderWidth: 1,
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                position: 'bottom',
                text: 'Daily Trend'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 80,
                        suggestedMax: 110
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Days'
                    }
                }]
            }
        }
    });
}