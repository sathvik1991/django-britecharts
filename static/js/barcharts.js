var endpoint = '/api/chart/data/psa'
var daily_data = [];
var daily_lable = [];
var months_data = [];
var months_lable = [];
var target;
var yearly_data = [];
var yearly_lable = [];
var year_target;
$.ajax({
    method: "GET",
    url: endpoint,
    success: function(data) {
        daily_lable = data.daily_lable
        daily_data = data.daily_data
        months_data = data.months_data
        months_lable = data.months_lable
        target = data.target
        year_data = data.year_data
        year_lable = data.year_lable
        year_target = data.year_target
        console.log(months_data)
        setChart()
    },
    error: function(error_data) {
        console.log("error")
        console.log(error_data)
    }
})

function setChart() {
    var YEAR = ["F15", "F16", "F17", "F18", "F19"];
    var color = Chart.helpers.color;
    var yearlydata = {
        labels: year_lable,
        datasets: [{
            label: 'Percentage',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            data: year_data
        }, {
            label: 'Target',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            data: year_target,
            type: 'line',
            fill: false,
        }]
    };
    var ctx = document.getElementById("yearlycanvas").getContext("2d");
    var yearlychart = new Chart(ctx, {
        type: 'bar',
        data: yearlydata,
        options: {
            showAllTooltips: false,
            elements: {
                rectangle: {
                    borderWidth: 1,
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                position: 'bottom',
                text: 'Yearly Trend'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 110
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Financial Year'
                    }
                }]
            }
        }
    });
    var montlyBarChartData = {
        labels: months_lable,
        datasets: [{
            label: 'Percentage',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            data: months_data
        }, {
            label: 'Target',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            data: Array(12).fill(target),
            type: 'line',
            fill: false,
        }]
    };
    var ctx1 = document.getElementById("montlycanvas").getContext("2d");
    var montlychart = new Chart(ctx1, {
        type: 'bar',
        data: montlyBarChartData,
        options: {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each horizontal bar to be 2px wide
            showAllTooltips: false,
            elements: {
                rectangle: {
                    borderWidth: 1,
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                position: 'bottom',
                text: 'Monthly Trend'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 80,
                        suggestedMax: 110
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }]
            }
        }
    });
    var dailyBarChartData = {
        labels: daily_lable,
        datasets: [{
            label: 'Percentage',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            data: daily_data,
        }, {
            label: 'Target',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            data: Array(30).fill(target),
            type: 'line',
            fill: false,
        }]
    };
    var dailyctx = document.getElementById("dailycanvas").getContext("2d");
    var dailychart = new Chart(dailyctx, {
        type: 'bar',
        data: dailyBarChartData,
        options: {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each horizontal bar to be 2px wide
            showAllTooltips: false,
            elements: {
                rectangle: {
                    borderWidth: 1,
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                position: 'bottom',
                text: 'Daily Trend'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 60,
                        suggestedMax: 110
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Days'
                    }
                }]
            }
        }
    });
}