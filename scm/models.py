"""SCM Department Models."""
from __future__ import unicode_literals
from django.db import models
import datetime
from django.db.models.signals import pre_save

# Create your models here.


class ScheduleAdherenceTarget(models.Model):
    """This is a Django model for Tracking targets.

    for Production Schedule Adherence."""
    
    YEAR_CHOICES = [(r,r) for r in range(2005, datetime.date.today().year+10)]
    # for r in range(2000, (datetime.datetime.now().year+1)):
    #     YEAR_CHOICES.append((r, r)

    financial_year = models.IntegerField(
        choices=YEAR_CHOICES,
        default=datetime.date.today().year+2,
        unique=True,
    )
    target = models.PositiveIntegerField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return str(self.financial_year)

    class Meta:
        """."""

        db_table = "ScheduleAdherenceTarget"
        ordering = ["financial_year"]


def convert_to_fyear(udate):
    """."""
    uyear = udate.year
    print "======", uyear
    myfyear = [str(uyear) + '-04-01', str(uyear + 1) + '-03-31']
    if myfyear[0] < str(udate) < myfyear[1]:
        print uyear
        return uyear + 1
    else:
        print "====", uyear
        return uyear


class ScheduleAdherence(models.Model):
    """This is a Django model for Producition Schedule Adherence."""

    date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        unique=True,
        blank=False,
        null=False,
        verbose_name="Date",
        help_text='',
    )
    value = models.PositiveIntegerField()
    remarks = models.CharField(
        max_length=200,
        verbose_name="Remarks.",
        help_text="Enter remarks if any",
        blank=True,
        null=False,
    )
    action_plane = models.CharField(
        max_length=200,
        verbose_name="Action Plane.",
        help_text="Enter Action if any",
        blank=True,
        null=True,
    )
    target = models.PositiveIntegerField(blank=True,)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        """."""

        db_table = "ScheduleAdherence"
        ordering = ["date"]


class Dbm(models.Model):
    """This is a Django model for DBM."""

    date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        unique=True,
        blank=False,
        null=False,
        verbose_name="Date",
        help_text='',
    )
    value = models.PositiveIntegerField()
    remarks = models.CharField(
        max_length=200,
        verbose_name="Remarks.",
        help_text="Enter remarks if any",
        blank=True,
        null=False,
    )
    action_plane = models.CharField(
        max_length=200,
        verbose_name="Action Plane.",
        help_text="Enter Action if any",
        blank=True,
        null=True,
    )
    target = models.PositiveIntegerField(blank=True,)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        """."""

        db_table = "Dbm"
        ordering = ["date"]


class DbmTarget(models.Model):
    """This is a Django model for Tracking targets.

    for DBM."""
    
    YEAR_CHOICES = [(r,r) for r in range(2005, datetime.date.today().year+10)]
    # for r in range(2000, (datetime.datetime.now().year+1)):
    #     YEAR_CHOICES.append((r, r)

    financial_year = models.IntegerField(
        choices=YEAR_CHOICES,
        default=datetime.date.today().year+2,
        unique=True,
    )
    target = models.PositiveIntegerField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return str(self.financial_year)

    class Meta:
        """."""

        db_table = "DbmTarget"
        ordering = ["financial_year"]


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.target:
        instance.target = convert_to_fyear(instance.date)


pre_save.connect(pre_save_post_receiver, sender=ScheduleAdherence)
pre_save.connect(pre_save_post_receiver, sender=Dbm)