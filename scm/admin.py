from django.contrib import admin

from .models import (
    ScheduleAdherence,
    ScheduleAdherenceTarget,
    Dbm,
    DbmTarget,
)
from import_export import resources
from import_export.admin import (
    ImportExportModelAdmin, 
    ExportActionModelAdmin,
)


# Register your models here.
class ScheduleAdherenceResource(resources.ModelResource):

    class Meta:
        model = ScheduleAdherence

class ScheduleAdherenceModelAdmin(
    ImportExportModelAdmin,
    ExportActionModelAdmin,
    admin.ModelAdmin):
    """."""

    list_display = [
        "date",
        "value",
        "remarks",
        "action_plane",
        "target",
        "updated",
    ]
    resource_class = ScheduleAdherenceResource

    class Meta:
        """."""

        model = ScheduleAdherence


class ScheduleAdherenceTargetResource(resources.ModelResource):

    class Meta:
        model = ScheduleAdherenceTarget


class ScheduleAdherenceTargetModelAdmin(
    ImportExportModelAdmin,
    ExportActionModelAdmin,
    admin.ModelAdmin):
    """."""

    list_display = [
        "pk",
        "financial_year",
        "target",
        "updated",
    ]
    resource_class = ScheduleAdherenceTargetResource

    class Meta:
        """."""

        model = ScheduleAdherenceTarget

class ScheduleAdherenceResource(resources.ModelResource):

    class Meta:
        model = ScheduleAdherence

class ScheduleAdherenceModelAdmin(
    ImportExportModelAdmin,
    ExportActionModelAdmin,
    admin.ModelAdmin):
    """."""

    list_display = [
        "date",
        "value",
        "remarks",
        "action_plane",
        "target",
        "updated",
    ]
    resource_class = ScheduleAdherenceResource

    class Meta:
        """."""

        model = ScheduleAdherence


class ScheduleAdherenceTargetResource(resources.ModelResource):

    class Meta:
        model = ScheduleAdherenceTarget


class ScheduleAdherenceTargetModelAdmin(
    ImportExportModelAdmin,
    ExportActionModelAdmin,
    admin.ModelAdmin):
    """."""

    list_display = [
        "pk",
        "financial_year",
        "target",
        "updated",
    ]
    resource_class = ScheduleAdherenceTargetResource

    class Meta:
        """."""

        model = ScheduleAdherenceTarget


class DbmResource(resources.ModelResource):

    class Meta:
        model = Dbm

class DbmModelAdmin(
    ImportExportModelAdmin,
    ExportActionModelAdmin,
    admin.ModelAdmin):
    """."""

    list_display = [
        "date",
        "value",
        "remarks",
        "action_plane",
        "target",
        "updated",
    ]
    resource_class = DbmResource

    class Meta:
        """."""

        model = Dbm


class DbmTargetResource(resources.ModelResource):

    class Meta:
        model = DbmTarget


class DbmTargetModelAdmin(
    ImportExportModelAdmin,
    ExportActionModelAdmin,
    admin.ModelAdmin):
    """."""

    list_display = [
        "pk",
        "financial_year",
        "target",
        "updated",
    ]
    resource_class = DbmTargetResource

    class Meta:
        """."""

        model = DbmTarget

admin.site.register(ScheduleAdherence, ScheduleAdherenceModelAdmin)
admin.site.register(ScheduleAdherenceTarget, ScheduleAdherenceTargetModelAdmin)
admin.site.register(Dbm, DbmModelAdmin)
admin.site.register(DbmTarget, DbmTargetModelAdmin)
