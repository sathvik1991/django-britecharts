"""SCM url's."""
from django.conf.urls import url
from .views import (
	# barcharts,
	ScheduleAdherenceCharts,
	DbmCharts,
	HomeView,
)


urlpatterns = [
	url(r'^$', HomeView.as_view(), name='home'),
    # url(r'barcharts', barcharts, name='barcharts'),
    url(r'^api/chart/data/psa$', ScheduleAdherenceCharts.as_view(), name='psa'),
    url(r'^api/chart/data/dbm$', DbmCharts.as_view(), name='dbm'),

]
