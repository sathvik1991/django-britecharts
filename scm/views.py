"""SCM Views."""
from django.shortcuts import render
from django.http import Http404
from django.db.models import Avg
import datetime
# Create your views here.
from scm.models import (
    ScheduleAdherence,
    ScheduleAdherenceTarget,
    Dbm,
    DbmTarget,
)
from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response


months_list = {
    1: "Apr",
    2: "May",
    3: "Jun",
    4: "Jul",
    5: "Aug",
    6: "Sept",
    7: "Oct",
    8: "Nov",
    9: "Dec",
    10: "Jan",
    11: "Feb",
    12: "Mar"
}


def get_financial_year(financial_year):
    """."""
    return [str(financial_year - 1) + "-04-01", str(financial_year) + "-03-31"]


def convert_to_fyear(udate):
    """."""
    uyear = udate.year
    print "======", uyear
    myfyear = [str(uyear) + '-04-01', str(uyear + 1) + '-03-31']
    if myfyear[0] < str(udate) < myfyear[1]:
        print uyear
        return uyear + 1, myfyear
    else:
        return uyear, [str(uyear - 1) + '-04-01', str(uyear) + '-03-31']


class ScheduleAdherenceCharts(APIView):
    """Schedule Adherence BarCharts."""

    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        """."""
        today = datetime.datetime.now()
        dates = ScheduleAdherence.objects.filter(
            date__year=today.year,
            date__month=today.month).values_list('date', flat=True)
        values = ScheduleAdherence.objects.filter(
            date__year=today.year,
            date__month=today.month).values_list('value', flat=True)
        target = (
            ScheduleAdherenceTarget.objects
            .filter(financial_year=today.year + 1)
            .values_list('target', flat=True)
        )
        # Daily graphs data is available here
        daily_lable = [i.day for i in dates]
        daily_data = values

        # Monthly graphs data is available here
        current_finacial_year = convert_to_fyear(today)[1]
        months_lable = []
        months_data = []
        for i in months_list:
            months = (
                ScheduleAdherence.objects
                .filter(date__range=current_finacial_year, date__month=i + 3)
                .aggregate(Avg('value'))
            )
            if i + 3 > today.month:
                break
            months_lable.append(months_list[i])
            months_data.append(months['value__avg'])
            # print months['value__avg']
        fyears = (
            ScheduleAdherenceTarget
            .objects.all()
            .values_list('financial_year', 'target')
        )
        financial_years = [i[0] for i in fyears]
        financial_target = [i[1] for i in fyears]
        yearly_data = []
        for year in financial_years:
            f_range = get_financial_year(int(year))
            year_data = (
                ScheduleAdherence.objects
                .filter(date__range=f_range)
                .aggregate(Avg('value'))
            )['value__avg']
            # yearly_data.append(year_data)
            yearly_data.append(year_data if year_data is not None else 0)
        
        data = {
            "daily_lable": daily_lable,
            'daily_data': daily_data,
            'months_lable': months_lable,
            'months_data': months_data,
            'target': target[0],
            'year_lable': financial_years,
            'year_target': financial_target,
            'year_data': yearly_data,
        }
        return Response(data)


class DbmCharts(APIView):
    """Schedule Adherence BarCharts."""

    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        """."""
        today = datetime.datetime.now()
        dates = Dbm.objects.filter(
            date__year=today.year,
            date__month=today.month).values_list('date', flat=True)
        values = Dbm.objects.filter(
            date__year=today.year,
            date__month=today.month).values_list('value', flat=True)
        target = (
          DbmTarget.objects
            .filter(financial_year=today.year + 1)
            .values_list('target', flat=True)
        )
        # Daily graphs data is available here
        daily_lable = [i.day for i in dates]
        daily_data = values

        # Monthly graphs data is available here
        current_finacial_year = convert_to_fyear(today)[1]
        months_lable = []
        months_data = []
        for i in months_list:
            months = (
                Dbm.objects
                .filter(date__range=current_finacial_year, date__month=i + 3)
                .aggregate(Avg('value'))
            )
            if i + 3 > today.month:
                break
            months_lable.append(months_list[i])
            months_data.append(months['value__avg'])
            # print months['value__avg']
        fyears = (
            DbmTarget
            .objects.all()
            .values_list('financial_year', 'target')
        )
        financial_years = [i[0] for i in fyears]
        financial_target = [i[1] for i in fyears]
        yearly_data = []
        for year in financial_years:
            f_range = get_financial_year(int(year))
            year_data = (
                Dbm.objects
                .filter(date__range=f_range)
                .aggregate(Avg('value'))
            )['value__avg']
            # yearly_data.append(year_data)
            yearly_data.append(year_data if year_data is not None else 0)
        
        dbm_data = {
            "daily_lable": daily_lable,
            'daily_data': daily_data,
            'months_lable': months_lable,
            'months_data': months_data,
            'target': target[0],
            'year_lable': financial_years,
            'year_target': financial_target,
            'year_data': yearly_data,
        }
        return Response(dbm_data)


class HomeView(View):
    """."""

    def get(self, request, *args, **kwargs):
        """."""
        return render(request, 'barcharts.html', {"customers": 10})
